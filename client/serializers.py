from .models import Client
from rest_framework import serializers


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ["first_name", "last_name", "id_number", "email", "balance"]
        read_only_fields = ["balance"]