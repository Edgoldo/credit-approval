from django.contrib import admin
from django.urls import path

urlpatterns = [
    path('admin/', admin.site.urls),
]


from django.urls import include, path
from rest_framework import routers
from client.views import ClientViewSet
from transaction.views import (
    TransactionAPIView, CreditRequestViewSet,
    CreditRequestUpdateView
)

router = routers.DefaultRouter()
router.register(r'clients', ClientViewSet)
# router.register(r'transactions', TransactionAPIView)
router.register(r'requests', CreditRequestViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('transactions/', TransactionAPIView.as_view(), name='transactions'),
    path('requests/update/<int:pk>', CreditRequestUpdateView.as_view(), name='requests_update'),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]