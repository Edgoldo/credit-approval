from rest_framework.exceptions import APIException


class InsufficientBalance(APIException):
    status_code = 400
    default_detail = "The client don't have enough balance."
    default_code = "insufficient_balance"