from django.db import models

from client.models import Client
from .consts import TRANSACTION_TYPE, CREDIT_STATUS, SENTINEL_INDICATOR


class Transaction(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    type = models.CharField(max_length=50, choices=TRANSACTION_TYPE)
    amount = models.DecimalField(max_digits=12, decimal_places=2)


class CreditRequest(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    amount = models.DecimalField(max_digits=12, decimal_places=2)
    status = models.CharField(max_length=50, choices=CREDIT_STATUS, default="CHECKING")


class SBSStatus(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    debt_amount = models.DecimalField(max_digits=12, decimal_places=2)
    

class SentinelStatus(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    indicator = models.CharField(max_length=50, choices=SENTINEL_INDICATOR)
