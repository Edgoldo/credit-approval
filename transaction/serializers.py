from .models import Transaction, CreditRequest
from rest_framework import serializers

from .utils import check_client_transactions, check_client_sbs, check_client_sentinel
from client.models import Client


class TransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transaction
        fields = ["client", "type", "amount"]


class CreditRequestSerializer(serializers.ModelSerializer):
    sbs_status = serializers.SerializerMethodField()
    sentinel_status = serializers.SerializerMethodField()
    total_status = serializers.SerializerMethodField()

    class Meta:
        model = CreditRequest
        fields = [
            "client", "created", "amount", "status",
            "sbs_status", "sentinel_status", "total_status"
        ]
        read_only_fields = ["status"]

    def get_sbs_status(self, obj):
        client = obj.client
        credit_amount = obj.amount
        sbs_status = check_client_sbs(client, credit_amount)
        return sbs_status

    def get_sentinel_status(self, obj):
        client = obj.client
        sentinel_status = check_client_sentinel(client)
        return sentinel_status

    def get_total_status(self, obj):
        client = obj.client
        credit_amount = obj.amount
        client_transactions = check_client_transactions(client)
        sbs_status = check_client_sbs(client, credit_amount)
        sentinel_status = check_client_sentinel(client)
        total_status = (client_transactions + sbs_status + sentinel_status)/10
        return total_status
