import random

from .models import Transaction, SBSStatus, SentinelStatus


def check_client_transactions(client):
    """
    Returns true if the transactions number of a client
    is ok.
    """
    ok_status = 10
    not_ok_status = random.randint(0, 5)
    transactions = Transaction.objects.filter(client=client)
    if transactions.count() == 0:
        return not_ok_status
    debit_transactions = transactions.filter(type="DEBIT")
    if transactions.count() - debit_transactions.count() < 0:
        return not_ok_status

    debit_transactions = transactions[:10]
    count = 0
    for transaction in debit_transactions:
        if transaction.type == "DEBIT":
            count += 1
        if count > 6:
            return not_ok_status

    return ok_status


def check_client_sbs(client, credit_amount):
    """
    Returns the amount of the debt of a client on SBS platform.
    """
    ok_status = 10
    regular_status = random.randint(2, 5)
    not_ok_status = random.randint(0, 5)
    sbs_status = SBSStatus.objects.filter(client=client).first()
    if sbs_status and getattr(sbs_status, "debt_amount") > 0:
        debt_amount = sbs_status.debt_amount
        if debt_amount < credit_amount:
            return regular_status
        else:
            return not_ok_status
    return ok_status


def check_client_sentinel(client):
    """
    Returns the sentinel indicator of a client.
    """
    ok_status = 10
    regular_status = random.randint(2, 5)
    not_ok_status = random.randint(0, 5)
    sentinel_status = SentinelStatus.objects.filter(client=client).first()
    if sentinel_status:
        sentinel_indicator = sentinel_status.indicator
        if sentinel_indicator == "BAD":
            return not_ok_status
        elif sentinel_indicator == "REGULAR":
            return regular_status

    return ok_status