from decimal import Decimal
from rest_framework import generics, mixins, viewsets
# from rest_framework import permissions

from .exceptions import InsufficientBalance
from .models import Transaction, CreditRequest
from .serializers import TransactionSerializer, CreditRequestSerializer


class TransactionAPIView(mixins.CreateModelMixin, generics.ListAPIView):
    lookup_field = "pk"
    queryset = Transaction.objects.all()
    serializer_class = TransactionSerializer
    # permission_classes = [permissions.IsAuthenticated]

    def perform_create(self, serializer):
        transaction = serializer.validated_data
        client = transaction.get("client")
        transaction_type = transaction.get("type")
        transaction_amount = Decimal(transaction.get("amount"))
        if transaction_type == "DEBIT":
            # Check if can make a debit to a client
            if client.balance < transaction_amount:
                raise InsufficientBalance()
            client.balance -= transaction_amount
        else:
            client.balance += transaction_amount
        client.save()

        serializer.save()

    def post(self, request, *args, **kwargs):
        """
        Method used for save a new register
        """
        return self.create(request, *args, **kwargs)


class CreditRequestViewSet(viewsets.ModelViewSet):
    queryset = CreditRequest.objects.all()
    serializer_class = CreditRequestSerializer
    # permission_classes = [permissions.IsAuthenticated]


class CreditRequestUpdateView(mixins.RetrieveModelMixin, mixins.UpdateModelMixin, generics.GenericAPIView):
    lookup_field = 'pk'
    queryset = CreditRequest.objects.all()
    serializer_class = CreditRequestSerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)
